
document.getElementById("exportButton").addEventListener("click", async () => {
    
    let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
  
    chrome.storage.local.get("baseUrl", ({ baseUrl }) => {               // https://jiraprod.duke-energy.com/secure/CreateIssueDetails!init.jspa
        chrome.storage.local.get("projectId", ({ projectId }) => {       // 23104
            chrome.storage.local.get("issueId", ({ issueId }) => {       // 10102   
                chrome.storage.local.get("priority", ({ priority }) => { // 3
                    chrome.storage.local.get("userId", ({userId}) => {
                        
                        chrome.scripting.executeScript({
                            target: { tabId: tab.id },
                            function: readInformation
                        }, (f) => {    
                                var fbData = f[0].result;

                                var summary = 'Firebase Crash - '+fbData.platform+' - '+fbData.title;
                                console.log(summary);

                                var url = '[Direct Link|'+fbData.url+']';
                                var plat = '*Platform:* '+fbData.platform;
                                var users = '*Users Affected:* '+fbData.usersAffected;     
                                var stackTrace = fbData.stackTraceTitle+"\n"+fbData.stackTraceSubTitle;                
                                var issueDescription = url+"\n\n"+plat+"\n"+users+"\n\n"+stackTrace;
                                console.log(issueDescription);            
                                
                                var projectIdUri = '?pid='+projectId;
                                var issueTypeUri = '&issuetype='+issueId;
                                var summaryUri = '&summary='+summary;
                                var descriptionUri = '&description='+encodeURIComponent(issueDescription);    
                                var priorityUri = '&priority='+priority;
                                var reporterUri = '&reporter='+userId;

                                var jiraLink = baseUrl+projectIdUri+issueTypeUri+summaryUri+descriptionUri+priorityUri+reporterUri;
                                console.log(jiraLink);

                                window.open(jiraLink,'_blank');
                        });
                        
                    });
                });
            });
        });
    });
    
});

function readInformation() {

    let FirebaseData = class {
        constructor(url, platform, title, usersAffected, stTitle, stSubTitle) {
            this.url = url;
            this.platform = platform;
            this.title = title;
            this.usersAffected = usersAffected;
            this.stackTraceTitle = stTitle;
            this.stackTraceSubTitle = stSubTitle;   
        }
    }

    var platform = document.getElementsByClassName('gmp-icons ng-star-inserted')[0].innerHTML.replace("plat_","").trim();
    var title = document.getElementsByClassName('fire-feature-bar-title')[0].innerHTML.trim()

    var usersAffectedHtml = document.getElementsByClassName('summary-sentence ng-star-inserted')[0].innerHTML
    var usersAffected = usersAffectedHtml.replaceAll("<b>","").replaceAll("</b>","").replaceAll("<span>","").replaceAll("</span>","").trim()

    var stackTraceTitle = document.getElementsByClassName('stack-trace-title')[0].innerHTML.trim()
    var stackTraceSubTitle = document.getElementsByClassName('stack-trace-subtitle')[0].innerHTML.trim()

    var fbData = new FirebaseData(document.URL, platform, title, usersAffected, stackTraceTitle, stackTraceSubTitle)
    console.log(fbData)

    return fbData
}
