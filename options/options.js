
window.onload = function() {

    chrome.storage.local.get("userId", ({userId}) => { 
        console.log("loaded userId: "+userId);
        document.getElementById('reportId').value = userId;
    });

    chrome.storage.local.get("projectId", ({projectId}) => { 
        console.log("loaded projectId: "+projectId);
        document.getElementById('projectId').value = projectId; 
    });

    chrome.storage.local.get("issueId", ({issueId}) => { 
        console.log("loaded issueId: "+issueId);
        document.getElementById('issueId').value = issueId; 
    });

    chrome.storage.local.get("baseUrl", ({baseUrl}) => { 
        console.log("loaded baseUrl: "+baseUrl);
        document.getElementById('jiraUrl').value = baseUrl; 
    });

    chrome.storage.local.get("priority", ({priority}) => {
        console.log("loaded priority: "+priority);
        document.getElementById('priority').value = priority;
    });

}

document.getElementById('saveButton').addEventListener('click', async () => {

    var userId = document.getElementById('reportId').value.trim();
    var projectId = document.getElementById('projectId').value.trim();
    var issueId = document.getElementById('issueId').value.trim(); 
    var baseUrl = document.getElementById('jiraUrl').value.trim(); 
    var priority = document.getElementById('priority').value.trim();

    chrome.storage.local.set({ userId });
    chrome.storage.local.set({ projectId });
    chrome.storage.local.set({ issueId });
    chrome.storage.local.set({ baseUrl });
    chrome.storage.local.set({ priority });

    showSnackBar('Save Successful');
}) 

document.getElementById('clearButton').addEventListener('click', async () => {

    document.getElementById('reportId').value = ''
    document.getElementById('projectId').value = '';
    document.getElementById('issueId').value = ''; 
    document.getElementById('jiraUrl').value = ''; 
    document.getElementById('priority').value = '';

    var userId = ''
    var projectId = ''
    var issueId = ''
    var baseUrl = ''
    var priority = ''

    chrome.storage.local.set({ userId });
    chrome.storage.local.set({ projectId });
    chrome.storage.local.set({ issueId });
    chrome.storage.local.set({ baseUrl });
    chrome.storage.local.set({ priority });

    showSnackBar('Clear Successful');
})

function showSnackBar(message) {
    var x = document.getElementById("snackbar");
    x.className = "show";
    x.innerHTML = message
    setTimeout(function() { 
        x.className = x.className.replace("show", ""); 
    }, 3000);
}