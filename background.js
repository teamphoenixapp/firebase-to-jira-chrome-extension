
const userId = "";
const projectId = "";
const issueId = "";
const baseUrl = "";
const priority = "3";

chrome.runtime.onInstalled.addListener(() => {
    chrome.storage.local.set({ userId });
    chrome.storage.local.set({ projectId });
    chrome.storage.local.set({ issueId });
    chrome.storage.local.set({ baseUrl });
    chrome.storage.local.set({ priority });

    console.log('userId set to '+userId);   
    console.log('projectId set to '+projectId);
    console.log('issueId set to '+issueId);
    console.log('baseUrl set to '+baseUrl);   
    console.log('priority set to '+priority);   
});
